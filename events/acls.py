from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    # Use the Pexels API
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    res = requests.get(url, headers=headers)
    pic_url = res.json()["photos"][0]["url"]
    return pic_url


def get_weather_data(city, state):
    # Use the Open Weather API
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    lat = res.json()[0]["lat"]
    lon = res.json()[0]["lon"]

    url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    res2 = requests.get(url2)
    weather = {
        "description": res2.json()["weather"][0]["description"],
        "temperature": res2.json()["main"]["temp"],
    }

    return weather
